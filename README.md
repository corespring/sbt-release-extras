# sbt-release-extras

A collection of extra utilities for use in conjunction with [sbt-release](http://github.com/sbt/sbt-release), to improve you're release workflow.


## Steps

### mergeCurrentBranchTo(target)

### tagBranchWithReleaseTag(target)

### prepareReleaseVersion

### checkBranchVersion

### runIntegrationTest

## Commands

### create-hotfix-branch

### create-release-branch

## Settings

### validHotfixParents : Seq[String] 

default: Seq("master")

### validReleaseParents : Seq[String]

default: Seq("develop")

### branchNameConverter : BranchNameConverter

default: FolderStyleConverter