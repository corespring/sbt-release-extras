lazy val `sbt-release-extras` = project in file(".")

organization := "org.corespring"
name := "sbt-release-extras"

sbtPlugin := true
publishMavenStyle := true
scalacOptions += "-deprecation"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

libraryDependencies ++= Seq("org.specs2" %% "specs2-core" % "3.6.5" % "test")

scalacOptions in Test ++= Seq("-Yrangepos")

//Add release as a library dependency also
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.0")

// Scripted
scriptedSettings

scriptedLaunchOpts <<= (scriptedLaunchOpts, version) { case (s,v) => s ++
  Seq("-Xmx1024M", "-XX:MaxPermSize=256M", "-Dplugin.version=" + v)
}

scriptedBufferLog := false

val cred = {
  val envCredentialsPath = System.getenv("CREDENTIALS_PATH")
  val path = if (envCredentialsPath != null) envCredentialsPath else Seq(Path.userHome / ".ivy2" / ".credentials").mkString
  val f: File = file(path)
  if (f.exists()) {
    println("[credentials] using credentials file")
    Credentials(f)
  } else {
    def repoVar(s: String) = sys.env.get("ARTIFACTORY_" + s)
    val args = Seq("REALM", "HOST", "USER", "PASS").map(repoVar).flatten
    if(args.length == 4){
      Credentials(args(0), args(1), args(2), args(3))
    } else {
      sys.error("can't find credentials")
    }
  }
}

credentials += cred

publishTo <<= version {
  (v: String) =>
    def isSnapshot = v.trim.contains("-")
    val base = "http://repository.corespring.org/artifactory"
    val repoType = if (isSnapshot) "snapshot" else "release"
    val finalPath = base + "/ivy-" + repoType + "s"
    Some("Artifactory Realm" at finalPath)
}
