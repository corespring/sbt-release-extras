package org.corespring.sbtrelease

import sbtrelease.Version

trait BranchNameConverter {
  def fromBranchName(branchName: String): Option[PrefixAndVersion]

  def toBranchName(pf: PrefixAndVersion): String
}

object FolderStyleConverter extends BranchNameConverter{
  val pattern = """^([^/]+)/([^/]+)$""".r

  override def fromBranchName(branchName: String): Option[PrefixAndVersion] = try {
    val pattern(prefix, versionString) = branchName
    Version(versionString).map{PrefixAndVersion(prefix, _)}
  } catch {
    case t : Throwable => None
  }

  override def toBranchName(pf: PrefixAndVersion): String = s"${pf.prefix}/${pf.version.withoutQualifier.string}"
}
