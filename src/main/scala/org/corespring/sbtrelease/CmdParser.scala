package org.corespring.sbtrelease

import scala.util.parsing.combinator.{JavaTokenParsers}


/* Parses string commands into Seq[String] so that they'll
 * run against [[sbt.Process]].
 * Eg: git commit . -m "hi" ==> Seq("git", "commit", ".", "-m", "\"hi\"")
 */
object CmdParser extends JavaTokenParsers{

  override def skipWhitespace = false

  def word: Parser[String] = """(\w|-|=|\/|\.)+""".r
  def dot : Parser[String] = """\.""".r

  def backTicked: Parser[String] = ("`"+"""([^"\p{Cntrl}\\]|\\[\\'"bfnrt]|\\u[a-fA-F0-9]{4})*"""+"`").r

  def piece : Parser[String] = word | stringLiteral | dot | backTicked
  def expr: Parser[List[String]] = rep1sep(piece, whiteSpace)

  def apply(input: String): Seq[String] = parseAll(expr, input.trim) match {
    case Success(result, _) => result
    case failure : NoSuccess => {
      println(failure)
      scala.sys.error(failure.msg)
    }
  }
}
