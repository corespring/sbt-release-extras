package org.corespring.sbtrelease

import sbtrelease.Version

private[sbtrelease] object Errors {

  def cantReadStringAsVersion(v:String) = sys.error(s"can't read '$v' as a semantic version, it should be in the form X.X.X")

  def unqualifiedVersionsDontMatch(git:Version,project:Version) = sys.error(s"Versions don't match: git: ${git.withoutQualifier.string}, project: ${project.withoutQualifier.string}")

  def cantReadBranchAsPrefixAndVersion(branch:String) = sys.error(s"unable to read the version from $branch")

  def cantBranch(validBranches:Seq[String]) = sys.error(s"You can only branch off of ${validBranches.mkString(", ")}")
}
