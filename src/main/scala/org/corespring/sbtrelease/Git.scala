package org.corespring.sbtrelease

import sbt._

import scala.util.{Failure, Success, Try}

private[sbtrelease] case class Git(baseDir:File){

  private def errorHandler(cmd:String) = s"[git] an error occured running: $cmd"

  def currentBranch = apply("rev-parse --abbrev-ref HEAD")

  def mostRecentTag = apply("describe --abbrev=0 --tags")

  def tag(t:String) =  apply(s"tag $t")

  def tags : Seq[String] = {
    val raw = apply("tag -l")
    raw.split("\n").map(_.trim)
  }

  def tagExists(t:String) : Boolean = try {
    apply(s"rev-parse $t")
    true
  } catch {
    case t : Throwable => false
  }

  def apply(cmd:String, onError : String => String) : String = {
    runGitProcess(onError)(cmd)
  }

  def apply(cmd:String) : String = apply(cmd, errorHandler _)
  def apply(cmd:String, onError : String = "[git] an error occured") : String = apply(cmd, _ => onError)

  def checkout(cmd:String, onError: String => String) : String = apply(s"checkout $cmd", onError)
  def checkout(cmd:String) : String = checkout(cmd, errorHandler _)

  def merge(cmd:String, onError: String => String) : String = apply(s"merge $cmd", onError)
  def merge(cmd:String) : String = merge(cmd, errorHandler _)

  private lazy val cmd = Seq("git")

  // in order to enable colors we trick git into thinking we're a pager, because it already knows we're not a tty
  val colorSupport: Seq[(String, String)] =
    if(ConsoleLogger.formatEnabled) Seq("GIT_PAGER_IN_USE" -> "1")
    else Seq.empty

  def defaultErrorMsg(cmd:String) = s"Nonzero exit code running git command: '$cmd'"

  private def runGitProcess(errorMsg : String => String)(cmd : String): String = {
    val log = ConsoleLogger()

    IO.createDirectory(baseDir)
    val full : Seq[String] = "git" +: CmdParser(cmd)
    log.debug(baseDir + "$ " + full.mkString(" "))

    Try {
      val out = Process(full, baseDir, colorSupport: _*).!!(log)
      out.trim()
    } match {
      case Success(s) => s
      case Failure(t) => {
        log.error(t.getMessage)
        sys.error(errorMsg(full.mkString(" ")))
      }
    }
  }
}

