package org.corespring.sbtrelease

import sbt.{Project, Extracted, State}
import sbt.Keys._

trait GitHelper {

  def withGitAndExtracted(st:State)(block: (Git,Extracted) => State) : State = {
    val extracted = Project.extract(st)
    val git: Git = Git(extracted.get(baseDirectory))
    git("diff --quiet", "The working tree is dirty")
    git("diff --cached --quiet", "The working tree is dirty")
    block(git, extracted)
  }
}
