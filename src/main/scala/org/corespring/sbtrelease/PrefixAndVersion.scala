package org.corespring.sbtrelease

import sbtrelease.Version

case class PrefixAndVersion(prefix: String, version: Version)
