package org.corespring.sbtrelease

import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport._
import sbtrelease.Version


object ReleaseExtrasPlugin extends AutoPlugin with GitHelper {

  /** Settings */
  val validHotfixParents = settingKey[Seq[String]]("A list of branches from which one can create a hotfix branch.")
  val validReleaseParents = settingKey[Seq[String]]("A list of branches from which one can create a release branch.")
  val branchNameConverter = settingKey[BranchNameConverter]("A converter for branch names, eg: 'release/0.0.1' <=> PrefixAndVersion('release', Version('0.0.1')).")

  /**
    * Note: this isnt configurable as we need to interoperate with sbt-release,
    * which uses v0.0.0 always for version tags.
    */
  lazy val tagConverter = VTagConverter

  /** Input tasks */
  lazy val checkProjectVersion = inputKey[Boolean]("Check the project version against the input")

  val checkProjectVersionTask = checkProjectVersion := {
    val args = sbt.complete.Parsers.spaceDelimited("<arg>").parsed
    val matches = Some(version.value) == args.headOption
    if (!matches) {
      sys.error(s"project version ${version.value} != ${args.headOption.getOrElse("?")}")
    }
    true
  }

  lazy val gitBranchIs = inputKey[Boolean]("Check the current git branch matches the input")

  val gitBranchIsTask = gitBranchIs := {
    val args = sbt.complete.Parsers.spaceDelimited("<arg>").parsed
    val extracted = Project.extract(state.value)
    val git: Git = Git(extracted.get(baseDirectory))
    val matches = Some(git.currentBranch) == args.headOption
    if (!matches) {
      sys.error(s"current branch ${git.currentBranch} != ${args.headOption.getOrElse("?")}")
    }
    true
  }

  val tagHelper = new Tags {}

  def getHotfixParents(git:Git, ex:Extracted) : Seq[Version] = {
    tagHelper.getLastPatchVersions(git.tags.flatMap(s => tagConverter.fromString(s)))
  }

  private def readHotfix(st:State)(options:Seq[Version], choiceLimit: Int) : Version = {

    val (choices, isLimited) = if(choiceLimit > 0 && choiceLimit < options.length ){
      options.take(choiceLimit) -> true
    } else options -> false

    val choiceString = choices.zipWithIndex.map{ case (v, index) => s"[$index]: ${v.string}" }.mkString("\n")

    val seeAll = if(isLimited) {
      s"""--
         |Only viewing the first $choiceLimit options, enter 'all' or 'a' to see all the choices.
         |
       """.stripMargin
    } else {
      ""
    }

    val manual =
      """--
        |If you want to manually enter the parent press 'm'.
      """.stripMargin
    val prompt =
      s"""
        |[Create Hotfix Branch]
        |Choose the parent of the new hotfix:
        |(Eg: if the parent is 1.0.0, the hotfix will be 1.0.1)
        |$choiceString
        |$seeAll
        |$manual
        |Enter a choice:
      """.stripMargin.trim

    SimpleReader.readLine(prompt) match {
      case Some(a) if(a == "a" || a == "all") => readHotfix(st)(options, 0)
      case Some("m") => SimpleReader.readLine("Enter version (no 'v'): ") match {
        case Some(v) => Version(v).getOrElse(sys.error(s"Can't read $v as a version"))
        case _ => sys.error("Nothing entered - aborting")
      }
      case Some(num) => try{
        val choice = num.toInt
        if(choices.length > choice){
          choices(choice)
        } else {
          sys.error(s"That index is out of bounds")
        }
      } catch {
        case t:Throwable => {
          st.log.debug(t.getMessage)
          sys.error(s"Can't parse $num as an int")
        }
      }
      case None => sys.error("Nothing entered - aborting")
    }
  }

  def normalize(v:Version) = {
    v.copy(minor = v.minor.orElse(Some(0)), bugfix = v.bugfix.orElse(Some(0)))
  }

  /** Commands */
  lazy val createHotfixBranch = Command.args(
    "create-hotfix-branch",
    "create a new branch 'hotfix/$hotfixVersion'"
  ) { (st: State, args:Seq[String]) =>

    withGitAndExtracted(st, validHotfixParents) { (git, ex) =>
      val branchConvert = ex.get(branchNameConverter)

      val eligibleParents = getHotfixParents(git, ex)

      def promptUser = {
        st.log.debug(s"eligibleParents: $eligibleParents")
        readHotfix(st)(eligibleParents, 3)
      }

      def parseUserVersion(s:String) = {
        Version(s).map(normalize)
          .getOrElse(sys.error(s"Can't read as version: $s"))
      }

      val rawParent = args match {
        case Nil => promptUser
        case s :: xs => parseUserVersion(s)
      }

      //1: Ensure that this tag exists as we need to use it as our parent
      val rawTag = tagConverter.toString(rawParent)

      if(!git.tagExists(rawTag)){
        sys.error(s"This tag doesn't exist: $rawTag, can't use it to build new branch")
      }

      val hotfixVersion = normalize(rawParent).bumpBugfix
      st.log.info(s"fix version: ${hotfixVersion.string}")

      val branchName = branchConvert.toBranchName(PrefixAndVersion("hotfix", hotfixVersion))

      git.checkout(s"-b $branchName $rawTag")
      val versionFile = ex.get(releaseVersionFile)
      writeVersion(versionFile, versionLine(hotfixVersion.asSnapshot))
      git( s"""add ${versionFile.getPath}""")
      git( s"""commit . -m "create hotfix - :arrow_up: ${hotfixVersion.string}" """)

      //Update the version in the project runtime too
      ReleaseTransformations.reapply(Seq(
        version in ThisBuild := hotfixVersion.asSnapshot.string), st
      )
    }
  }

  lazy val createReleaseBranch = Command.command("create-release-branch") { (st: State) =>
    withGitAndExtracted(st, validReleaseParents) { (git, ex) =>
      val projectVersionString = ex.get(Keys.version)

      Version(projectVersionString).map { projectVersion =>
        val nextVersion = projectVersion.bumpMinor.asSnapshot

        st.log.info(s"createReleaseBranch nextVersion=${nextVersion.string}, current project version=${projectVersion.string}")

        val currentBranch = git.currentBranch
        val versionFile = ex.get(releaseVersionFile)
        val branchConvert = ex.get(branchNameConverter)
        val releaseBranchName = branchConvert.toBranchName(PrefixAndVersion("release", projectVersion.withoutQualifier))

        git.checkout(s"-b $releaseBranchName")
        git.checkout(currentBranch)
        writeVersion(versionFile, versionLine(nextVersion))
        git(s"""add ${versionFile.getPath}""")
        git(s"""commit . -m ":arrow_up: ${nextVersion.string}" """)
        git.checkout(releaseBranchName)
        st
      }.getOrElse(Errors.cantReadStringAsVersion(projectVersionString))
    }
  }

  private def writeVersion(file: File, versionString: String) = IO.write(file, versionString)

  private def versionLine(v: Version) = s"""version in ThisBuild := "${v.string}" """.trim

  protected def withGitAndExtracted(st: State, validParents: SettingKey[Seq[String]])(block: (Git, Extracted) => State) : State = {
    withGitAndExtracted(st){ (git, extracted) =>
      val parents = extracted.get(validParents)
      if (!parents.contains(git.currentBranch)) {
        Errors.cantBranch(parents)
      }
      block(git, extracted)
    }
  }

  private def readVersion(prompt: String): Version = {
    SimpleReader.readLine(prompt) match {
      case Some(input) => Version(input).getOrElse {
        Errors.cantReadStringAsVersion(input)
      }
      case None => sys.error("No version provided!")
    }
  }

  //automatically add sbtrelease-extras to the build
  override def trigger = allRequirements

  override lazy val projectSettings: Seq[Setting[_]] = Seq(
    gitBranchIsTask,
    checkProjectVersionTask,
    validHotfixParents := Seq("master"),
    validReleaseParents := Seq("develop"),
    branchNameConverter := FolderStyleConverter,
    commands ++= Seq(createReleaseBranch, createHotfixBranch))

}
