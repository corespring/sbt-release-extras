package org.corespring.sbtrelease

import sbt._
import sbt.Keys._
import sbtrelease.ReleasePlugin.autoImport
import sbtrelease.ReleasePlugin.autoImport.{ReleaseKeys, ReleaseStep}
import sbtrelease.Version


object ReleaseSteps extends GitHelper{

  /**
    * Merge current branch to target. Stay on the target branch.
    */
  def mergeCurrentBranchTo(target:String) = ReleaseStep(action = (st:State) => {
    withGitAndExtracted(st){ (git, extracted) =>
      val branch = git.currentBranch
      git.checkout(target)
      git.merge(s"$branch -X theirs")
      st
    }
  })

  /**
    * Tag the target branch with the prepared releaseTagName (see sbt-release).
    * @param target
    * @return
    */
  def tagBranchWithReleaseTag(target:String) = ReleaseStep(action = (st:State) => {
    withGitAndExtracted(st){ (git, extracted) =>
      val (_, tag) = extracted.runTask(autoImport.releaseTagName, st)
      st.log.info(s"found tag: $tag")
      if(git.currentBranch != target){
        git.checkout(target)
      }
      git.tag(s"$tag")
      st
    }
  })

  lazy val runIntegrationTest: ReleaseStep = ReleaseStep(
    action = { st: State =>
      val extracted = Project.extract(st)
      val ref = extracted.get(thisProjectRef)
      st.log.info(s"[runIntegrationTest] - running...")
      extracted.runAggregated(test in IntegrationTest in ref, st)
  })

  lazy val prepareHotfixReleaseVersion = prepareReleaseVersion

  /**
    * Set just the release version so that sbt-release
    * will set it correctly in [[sbtrelease.ReleaseStateTransformations.setReleaseVersion]].
    * skip setting the next version as this won't be used
    */
  lazy val prepareReleaseVersion = ReleaseStep(action = (st) => {
    val extracted = Project.extract(st)
    val versionString = extracted.get(version)
    Version(versionString).map{ v =>
      st.log.info(s"setting the release version to ${v.withoutQualifier.string}")
      val update = st.put(ReleaseKeys.versions, (v.withoutQualifier.string, "not-used"))
      st.log.info(s"set to: ${update.get(ReleaseKeys.versions)}")
      update
    }.getOrElse(sys.error(s"Can't parse version: $versionString"))
  })

  /**
    * Check that the branch matches the target release version.
    * IE: you've created a hotfix branch off of 1.0.0 -> hotfix/1.0.1
    * This step will check that the version is 1.0.1-SNAPSHOT
    */
  lazy val checkBranchVersion : ReleaseStep = ReleaseStep( action = (st : State) => {
    val extracted = Project.extract(st)
    val git = Git(extracted.get(baseDirectory))
    val branchConvert = extracted.get(ReleaseExtrasPlugin.branchNameConverter)

    val currentBranch = git.currentBranch
    st.log.debug(s"check-branch: current git branch: $currentBranch")

    import Errors._

    branchConvert.fromBranchName(git.currentBranch).map{ pv =>
      val gitVersion = pv.version
      val projectVersion = Version(extracted.get(version)).getOrElse(cantReadStringAsVersion(extracted.get(version)))
      if(gitVersion.withoutQualifier != projectVersion.withoutQualifier){
        unqualifiedVersionsDontMatch(gitVersion, projectVersion)
      }
    }.getOrElse(cantReadBranchAsPrefixAndVersion(git.currentBranch))
    st
  })
}
