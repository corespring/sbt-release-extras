package org.corespring.sbtrelease

import sbtrelease.Version

trait TagConverter {
  def fromString(tag: String): Option[Version]
  def toString(v:Version): String
}

object RawTagConverter extends TagConverter{
  override def toString(v: Version): String = v.string

  override def fromString(tag: String): Option[Version] = Version(tag.substring(1))
}

/**
  * This is the tagging format that sbtrelease uses, it's not configuable in version 1.0.0
  */
object VTagConverter extends TagConverter{
  override def toString(v: Version): String = s"v${v.string}"

  override def fromString(tag: String): Option[Version] = if(tag.startsWith("v")){
    Version(tag.substring(1))
  } else None
}