package org.corespring.sbtrelease

import sbtrelease.Version

private object Tags{
  implicit object VersionOrdering extends Ordering[Version]{
    override def compare(x: Version, y: Version): Int = {
      x.string.compareTo(y.string)
    }
  }
}

trait Tags {

  import Tags._

  /**
    * Get the last patch version sorted by most recent.
    * Eg: 1.9.1, 1.9.2, 2.0.1, 2.0.3 returns: 1.9.2, 2.0.3
    * Note: only unqualified versions are used.
    */
  def getLastPatchVersions(v:Seq[Version]) : Seq[Version] = {

    def majorAndMinor(v:Version) = {
      v.copy(major = v.major, minor = v.minor, bugfix = None, qualifier = None).string
    }

    def unqualified(v:Version) = v.qualifier.isEmpty

    val grouped = v.filter(unqualified).groupBy(majorAndMinor).mapValues(_.sorted)
    val latest = grouped.flatMap{ case (_, versions) => versions.lastOption}.toSeq
    latest.sorted.reverse
  }
}
