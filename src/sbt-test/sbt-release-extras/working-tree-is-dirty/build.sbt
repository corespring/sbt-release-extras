name := "create-release-branch"

organization := "com.example"

lazy val root = (project in file("."))

releaseVersionBump := sbtrelease.Version.Bump.Minor

