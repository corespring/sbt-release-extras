package org.corespring.sbtrelease

import org.specs2.mutable.Specification

class CmdParserTest extends Specification {

  override def is = s2"""
    CmdParser
      parses "Hello" $e1
      parses "Hello world" $e2
      parses "." $e3
      parses "git checkout -b blah/0.1" $e4
      parses "commit . -m \":arrow_up: Version(5)\" $e5
      parses `...` $e6
    """

  def assert(s:String, expected:List[String]) = CmdParser(s) must_== expected

  def e1 = CmdParser("Hello") must_== List("Hello")
  def e2 = CmdParser("Hello world") must_== List("Hello", "world")
  def e3 = CmdParser(".") must_== List(".")
  def e4 = CmdParser("checkout -b blah/0.1") must_== List("checkout", "-b", "blah/0.1")
  def e5 = CmdParser("git commit . -m \":arrow_up: Version(5)\"") must_== List("git", "commit", ".", "-m", "\":arrow_up: Version(5)\"")
  def e6 = CmdParser("describe --tags `git hi`") must_== List("describe", "--tags", "`git hi`")
}
