package org.corespring.sbtrelease

import org.specs2.mutable.Specification
import sbtrelease.Version

class FolderStyleConverterTest extends Specification {

  override def is = s2"""
    FolderStyleConverter
      return PrefixAndVersion for "foo/0.0.1" $e1
      return None for 'foo/foo/0.0.1' $e2
      returns None for 'foo/bar' $e3
    """

  def assert(s:String, expected:List[String]) = CmdParser(s) must_== expected

  def e1 = FolderStyleConverter.fromBranchName("foo/0.0.1") must_== Some(PrefixAndVersion("foo", Version("0.0.1").get))
  def e2 = FolderStyleConverter.fromBranchName("foo/foo/0.0.1") must_== None
  def e3 = FolderStyleConverter.fromBranchName("foo/bar") must_== None
}
