package org.corespring.sbtrelease

import org.specs2.mutable.Specification
import sbtrelease.Version

object VersionHelpers{

  implicit class VersionsHelper(val sc:StringContext) extends AnyVal{
    def vs(args:Any*) : Seq[Version] = {
      sc.raw(args: _*).split(",").map(_.trim).flatMap(Version(_))
    }
  }
}

class TagsTest extends Specification with Tags{

  import VersionHelpers._

  def p(versions:Seq[Version]) = getLastPatchVersions(versions)

  def f(in:Seq[Version], expected:Seq[Version]) = {
    def mks(vs:Seq[Version]) = vs.map(_.string).mkString(", ")
    s"return ${mks(expected)} from ${mks(in)}" in {
      p(in) must_== expected
    }
  }

  "getLastPatchVersion" should {
    f(vs"0.0.0, 0.0.1", vs"0.0.1")
    f(vs"0.0.0, 0.0.1, 2.1.0, 2.2.0", vs"2.2.0, 2.1.0, 0.0.1")
    f(vs"2.1.0, 2.2.0, 0.0.1, 0.0.0", vs"2.2.0, 2.1.0, 0.0.1")
    f(vs"2.1, 2.2", vs"2.2, 2.1")
    f(vs"2.1.0-RC3, 2.1.0", vs"2.1.0")
  }
}
