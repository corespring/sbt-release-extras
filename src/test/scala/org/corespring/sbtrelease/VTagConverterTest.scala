package org.corespring.sbtrelease

import org.specs2.mutable.Specification
import sbtrelease.Version

class VTagConverterTest extends Specification {

  override def is = s2"""
    VTagConverterTest
    return Version for "v0.0.1" $e1
    return None for "vanguard" $e2
    return None for "0.0.1" $e3
    return None for "version-0.0.1" $e4
  """

  def assert(s:String, expected:List[String]) = CmdParser(s) must_== expected

  def e1 = VTagConverter.fromString("v0.0.1") must_== Some(Version("0.0.1").get)
  def e2 = VTagConverter.fromString("vanguard") must_== None
  def e3 = VTagConverter.fromString("0.0.1") must_== None
  def e4 = VTagConverter.fromString("version-0.0.1") must_== None

}
